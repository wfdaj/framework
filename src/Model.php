<?php

namespace mini;

class Model
{
    // 数据表名
    public $table        = null;

    // 数据表主键
    public $primaryKey   = null;

    // 模型对象
    public static $obj   = null;

    // 模型名称
    public static $mname = null;

    // 数据操作对象
    public $model        = null;

    // 数据操作错误信息
    public $error        = null;

    // 缓存对象
    protected $cacher    = null;

    // 构造函数用于初始化获取数据表操作对象
    public function __construct($connectDB = true)
    {
        if ($this->table != null && $connectDB) {
            $this->model = db($this->table);
        }
    }

    // 连接数据库
    public function connectDB()
    {
        $this->model = db($this->table);
    }

    // 利用 id 查询一条数据
    public function findById($id, $fields = '*')
    {
        return $this->model->where($this->primaryKey . ' = ?', array($id))->fetch($fields);
    }

    // 获取刚刚运行的 sql 语句
    public function getSql()
    {
        return $this->model->getSql();
    }

    // 获取 数据操作过程中产生的错误信息
    public function error()
    {
        return $this->model->error();
    }

    // 在模型内实现缓存 - 获取缓存对象
    // protected function getCacher()
    // {
    //     if (!empty($this->cacher)) {
    //         return null;
    //     }

    //     $config = config('cache');
    //     if (empty($config)) {
    //         throw new Exception("缓存设置错误");

    //     }

    //     if (!in_array($config['type'], config('allowCacheType'))) {
    //         throw new Exception('缓存类型错误');
    //     }

    //     $type           = strtolower($config['type']);
    //     $className      = 'mini\\Caches\\' . $type . 'Cacher';
    //     $this->cacher   = $className::getInstance($config);
    // }

    // 在模型内设置缓存
    // 设置并获取缓存数据
    // public function cache($name, $parameter = null, $queryMethod, $timer = 3600, $isSuper = true)
    // {
    //     if (!config('cache.start')) {
    //         return $this->$queryMethod();
    //     }

    //     $this->getCacher();
    //     $name      = setCacheName($name, $parameter, $isSuper);
    //     $cachedRes = $this->cacher->get($name);
    //     if ($cachedRes) {
    //         return $cachedRes;
    //     }

    //     $queryRes = $this->$queryMethod();
    //     if (empty($queryRes)) {
    //         return $queryRes;
    //     }

    //     $this->cacher->set($name, $queryRes, $timer);
    //     return $queryRes;
    // }

    // 模型内清除指定缓存
    // public function removeCache($name, $parameter = null, $isSuper = true)
    // {
    //     $this->getCacher();
    //     $name = setCacheName($name, $parameter, $isSuper);
    //     $this->cacher->removeCache($name);
    // }
}
